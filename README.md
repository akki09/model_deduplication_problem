# model_deduplication_problem

**Capable of running on both Rstudio and version of R 3.4.3 or upgraded version**

***IN R-STUDIO*** For file reading goto Session->Set Working Directory->Choose Directory...->**Then browse to the folder where sample input excel file is present for ex. G:\study\Innovaccer hackercamp**->Click Open  

**No pre-requisite installation of library packages required since all the functions are of base library**

*Input file must be named **"Deduplication Problem - Sample Dataset.csv"** *

*The model will create an output file named **"model_result_output.csv"** on the same folder as the input excel file*